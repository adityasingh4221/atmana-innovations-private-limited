8. Scenario:
a. You are given admin access to one AWS account which runs production
workloads
b. Question
i. What all steps you will review to ensure account and its services are
running securely

--------------------
answer 

- every IAM user should changing its secret key and access key every 3 months 
- monitor cloudTrail to see the logs for the services IAM  user using  and the access key which is used to access the service.
- monitor cloudwatch and dashboards to monitor load on servers
