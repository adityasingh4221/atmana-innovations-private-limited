7. Scenario:
a. You need to install and setup docker on 10 ec2 instances, IP of those instances
are already known.
b. You also need to create a user called “deploybot” on each host, and that user
should be able to run all docker commands without using sudo
c. Question
i. What tools you will use, write and describe tools you choose, and a
working script/solution which when executed perform above 2 tasks


------------------------------------------------------------------
answer 

- will create a ansible playbook for this task 
   1. assumption 
         - password less authentication is setup b/w my local machine and all 10 ec2 instance 
         - ec2 instance is ubuntu-18.04 .
         
   2. In /etc/ansible.cfg we have specify the inventory file  
       - inventory      = /home/ubuntu/ansible/hosts
       - [privilege_escalation]
           become=True
           become_method=sudo
           become_user=root
       - by default ansible look for same user who is pushing the playbook file , if our remote user is not same as on our local we can change it in 
          remote_user = bitnami
       - set the environment variable ANSIBLE_CONFIG  in the .bashrc file
            export ANSIBLE_CONFIG=/home/ubuntu/ansible/ansible.cfg

