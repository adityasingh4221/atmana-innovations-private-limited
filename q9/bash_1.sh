# bash file to count the number of times a status code is coming
# status code       occurance
# 200 -            9132
# 304 -            447
# 404 -            213
# 301 -            166
# 206 -            108
# 500 -            3
# 403 -             2
while read key 
do
   cnt=$(grep -w "$key" apache_logs|wc -l)
   pad="$key - $cnt"
   echo $pad
done < word.txt