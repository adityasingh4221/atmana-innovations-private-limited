#   uniq  -c =  It tells how many times a line was repeated by displaying a number as a prefix with the line
# sort -nr = To sort a file with numeric data in reverse order
#        -n  = to sort a file in  order small value to large value  

# 3 most occuring ip

# 482    66.249.73.135
# 364     46.105.14.53
# 357  130.237.218.86

cat apache_logs | awk '{print $1}' | sort -n | uniq  -c | sort -nr | head -20
